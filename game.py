from random import randint

# Ask for name
name = input("Hi! What is your name?")

for guess_number in range(1, 6):
# Set the month and year for randint
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)


    print("Guess", guess_number, ": were you born in",
        month_number, "/", year_number, "?")

    # response to guess
    response = input("yes or no? ")

    # check response for guess
    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Good bye")
    else:
        print("Drat! Lemme try again!")
